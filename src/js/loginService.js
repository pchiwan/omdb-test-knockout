
var LoginService = function () {

	var apiUrl = 'http://play.astrolab.io:4100/api/';
	
	this.getUser = function (username) {
		
		var url = '{0}users/username/{1}'.format(apiUrl, username);

		return $.ajax({
			url: url,
			dataType: 'json'
		})		
		.error(console.error);
	};
	
	this.createUser = function (user) {
		
		var url = '{0}users'.format(apiUrl);
		
		return $.ajax({
			url: url,
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(user)
		})
		.then(function (data) {
			return data && data.ops && data.ops.length
				? data.ops[0]
				: null
		});
	};
};