/// <reference path="../bower_components/knockout/dist/knockout.js" />

var ApplicationViewModel = function () {
	
	var self = this,
		omdbService = new OmdbService(),
		loginService = new LoginService(),
		reviewsService = new ReviewsService(),		
		$loginModal = $('#Login'),
		$logoutModal = $('#Logout'),
		KEY_ENTER = 13;

	//#region Public properties

	this.searchText = ko.observable('');
	this.results = ko.observableArray([]);	
	this.reviews = ko.observable([]);
	this.orderBy = ko.observable('');
	this.orderDesc = ko.observable(false);
	this.isLoadingTop = ko.observable(false);
	this.isLoadingBottom = ko.observable(false);
	this.isLoadingUser = ko.observable(false);
	this.showCreateUserButton = ko.observable(false),
	this.loginVisible = ko.observable(true);
	this.createNewUser = ko.observable(false);
	this.detail = ko.observable(null);
	this.user = new UserModel();
	this.newReview = new ReviewModel();
	
	this.hasResults = ko.computed(function () {
		return self.results().length > 0;
	});

	this.hasDetail = ko.computed(function () {
		return self.detail() != null;
	});
	
	this.hasReviews = ko.computed(function () {
		return self.reviews().length > 0;
	});

	this.currentImdbId = ko.computed(function () {
		return self.hasDetail()
			? self.detail().imdbID
			: '';
	});

	this.detailHasPoster = ko.computed(function () {		
		return self.hasDetail()
			&& !!self.detail().Poster 
			&& self.detail().Poster.indexOf('.jpg') > 0;	
	});

	this.detailTitle = ko.computed(function () {
		return self.hasDetail()
			? '{0} ({1})'.format(self.detail().Title, self.detail().Year)
		 	: '';
	});

	this.showLoginButton = ko.computed(function() {
		return !self.createNewUser() && !self.isLoadingUser() && self.user._id() === null;	
	});

	this.showCreateUserButton = ko.computed(function() {
		return self.createNewUser() && !self.isLoadingUser() && self.user._id() === null;	
	});

	//#endregion

	//#region Public methods

	this.search = function () {
		
		if (!self.searchText()) return;

		self.isLoadingTop(true);
		self.results([]);
		
		omdbService.search(self.searchText()).then(function (data) {			
			if (!!data && data.hasOwnProperty('Search')) {
				self.results(data.Search);
				self.sort('Year', true);
			}
			self.detail(null);
			self.isLoadingTop(false);
		});								
	};
	
	this.searchKeyup = function (data, event) {

		if (event.keyCode == KEY_ENTER) { // enter
			self.search();
		}
	};

	this.getById = function (id) {
		
		self.isLoadingBottom(true);

		omdbService.getById(id).then(function (data) {
			self.detail(data);			
			self.isLoadingBottom(false);
			self.getReviews(id);
		});
	};
	
	this.getReviews = function (id) {
		
		self.reviews([]);
		self.newReview.reset();
		
		reviewsService.getByImdbId(id).then(function (data) {
			self.reviews(_.map(data, function (x) {
				return new ReviewModel(x)
			}));
			self.isLoadingBottom(false);			
		});
	};
	
	this.isSorted = function (property) {
		
		return self.orderBy() == property;	
	};
	
	this.sort = function (orderBy, forceSort) {
	
		if (orderBy == self.orderBy() && !forceSort) {
			self.orderDesc(!self.orderDesc());
		} else {
			self.orderBy(orderBy);
			self.orderDesc(false);
		}
		var sorted = _.sortBy(self.results(), self.orderBy());		
		self.results(self.orderDesc() ? sorted.reverse() : sorted);
	};
	
	this.publishReview = function () {
	
		if (!!self.newReview.userReview()) {
			
			self.newReview.userId(self.user._id());
			self.newReview.username(self.user.name());
			self.newReview.imdbId(self.currentImdbId());
			
			reviewsService.createReview(self.newReview.raw()).then(function (data) {
				if (!!data) {
					self.newReview.reset();
					self.getReviews(self.currentImdbId());
				}
			});
		}
	};
	
	this.loginKeyup = function (data, event) {

		if (event.keyCode == KEY_ENTER) { // enter
			self.getUser();
		}		
	}
	
	this.getUser = function () {
	
		if (!!self.user.username()) {
			self.isLoadingUser(true);
			loginService.getUser(self.user.username()).then(function (data) {				
				if (!$.isEmptyObject(data)) {
					completeLogin(data);
				} else {
					self.isLoadingUser(false);
					self.createNewUser(true);					
				}
			});
		}
	};
	
	this.createUser = function () {
	
		if (!!self.user.username() && !!self.user.name()) {
			self.isLoadingUser(true);
			loginService.createUser(self.user.raw()).then(function (data) {				
				if (!$.isEmptyObject(data)) {
					completeLogin(data);
				}
			});
		}
	};
	
	this.logout = function () {
		
		var d = new Date();

		d.setDate(d.getDate() - 1);
		document.cookie = "username=; expires={0}; path=/".format(d.toUTCString());

		self.loginVisible(true);

		centerVertically($logoutModal);
		
		$logoutModal.modal({
			keyboard: false,
			backdrop: 'static',
			show: false
		});	
		
		$logoutModal.modal('show');
	};

	this.reload = function () {

		location.reload();
	};
	
	//#endregion
	
	//#region Private methods
	
	var completeLogin = function (data) {
		self.user._id(data._id);
		self.user.name(data.name);
		
		if (self.loginVisible()) {
			var d = new Date();
			d.setDate(d.getDate() + 1);					
			document.cookie = "username={0}; expires={1}; path=/".format(self.user.username(), d.toUTCString());

			setTimeout(function () {
				$loginModal.modal('hide');
				self.loginVisible(false);
			}, 2000);
		} else {
			self.loginVisible(false);
		}
	};

	var centerVertically = function ($modal) {

		$modal.on('show.bs.modal', function (e) {
			$modal.css('display', 'block');
			var $content = $modal.find('.modal-content'),
				top = Math.round(($modal.height() - $content.height()) / 2);
				
		    $content.css('margin-top', top > 0 ? top : 0);
		});
	};

	var init = function () {
		
		if (!!document.cookie && document.cookie.contains('username') >= 0) {
			var cookies = document.cookie.split(';');
			var cookie = _.find(cookies, function (x) { return x.contains('username'); });
			
			if (!!cookie) {
				var username = cookie.split('=')[1];
				username = username.replace('""', '');
				if (!!username) {
					self.user.username(username);
					self.loginVisible(false);
					self.getUser();
					return;
				}
			}
		}

		centerVertically($loginModal);
		
		$loginModal.modal({
			keyboard: false,
			backdrop: 'static',
			show: false
		});	
		
		$loginModal.modal('show');
	};
	
	//#endregion
	
	init();
};

ko.applyBindings(new ApplicationViewModel());
