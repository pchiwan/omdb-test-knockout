
var OmdbService = function () {
	
	var apiUrl = "http://www.omdbapi.com/?r=json&apikey={0}&";
	var APIKEY = 'e581c30d'; // this is MY API key, I paid for it!
		
	this.search = function (searchText) {
		
		var url = '{0}s={1}'.format(apiUrl.format(APIKEY), searchText);
		
		return $.ajax({
			url: url,
			dataType: 'json'
		})	
		.fail(function (data) {
			console.log(data);
		});
	};
	
	this.getByTitle = function (title) {
		
		var url = '{0}t={1}'.format(apiUrl.format(APIKEY), title);
		
		return $.ajax({
			url: url,
			dataType: 'json'
		})	
		.fail(function (data) {
			console.log(data);
		});
	};
	
	this.getById = function (id) {
		
		var url = '{0}i={1}'.format(apiUrl.format(APIKEY), id);
		
		return $.ajax({
			url: url,
			dataType: 'json'
		})	
		.fail(function (data) {
			console.log(data);
		});
	};
};
