
var ReviewModel = function (data) {

	var self = this;
	
	this._id = ko.observable(!!data && data.hasOwnProperty('_id') ? data._id : null);
	this.created = ko.observable(!!data && data.hasOwnProperty('created') ? new Date(data.created) : null);
	this.imdbId = ko.observable(!!data && data.hasOwnProperty('imdbId') ? data.imdbId : '');
	this.score = ko.observable(!!data && data.hasOwnProperty('score') ? data.score : 0);
	this.userId = ko.observable(!!data && data.hasOwnProperty('userId') ? data.userId : null);
	this.username = ko.observable(!!data && data.hasOwnProperty('username') ? data.username : '');
	this.userReview = ko.observable(!!data && data.hasOwnProperty('userReview') ? data.userReview : '');

	this.raw = function () {
		return {
			userId: self.userId(),
			username: self.username(),
			imdbId: self.imdbId(),
			score: self.score(),
			userReview: self.userReview(),
			created: Date()
		};	
	};

	this.reset = function () {
		this._id(null);
		this.created(null);
		this.imdbId('');
		this.score(0);
		this.userId(null);
		this.username('');
		this.userReview('');
	};
};

var UserModel = function (data) {

	var self = this;
	
	this._id = ko.observable(!!data && data.hasOwnProperty('_id') ? data._id : null);
	this.name = ko.observable(!!data && data.hasOwnProperty('name') ? data.name : null);
	this.username = ko.observable(!!data && data.hasOwnProperty('username') ? data.username : '');
	
	this.raw = function () {
		return {
			name: self.name(),
			username: self.username()
		};
	};
};