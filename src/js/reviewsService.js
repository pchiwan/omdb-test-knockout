
var ReviewsService = function () {
	
	var apiUrl = 'http://play.astrolab.io:4100/api/';
		
	this.getByUserId = function (userId) {
		
		var url = '{0}reviews/userid/{1}'.format(apiUrl, userId);

		return $.ajax({
			url: url,
			dataType: 'json'
		})		
		.error(console.error);
	};

	this.getByImdbId = function (imdbId) {
	
		var url = '{0}reviews/imdbid/{1}'.format(apiUrl, imdbId);

		return $.ajax({
			url: url,
			dataType: 'json'
		})	
		.error(console.error);
	};		
	
	this.createReview = function (review) {
	
		var url = '{0}reviews'.format(apiUrl);
		
		return $.ajax({
			url: url,
			type: 'POST',
			contentType: 'application/json',			
			data: JSON.stringify(review)
		});
	};
};
