/// <reference path="../bower_components/knockout/dist/knockout.js" />

var template = document.getElementById('score-component');

ko.components.register('score', {
	viewModel: function (params) {
		
		var self = this;

		this.name = params.name;
		this.score = params.score;
		this.disabled = params && params.hasOwnProperty('disabled') ? params.disabled : false;
		
		this.scoreOptions = [];		
		
		this.changeScore = function (score) {
			if (!self.disabled) {
				self.score(score);
			}
		};

		this.hoverIn = function(data, event) {
			if (!self.disabled) {
				var $target = $(event.target);
				$target.prevAll().andSelf().addClass('hovered');
			}
		};

		this.hoverOut = function(data, event) {
			if (!self.disabled) {
				var $target = $(event.target);
				$target.prevAll().andSelf().removeClass('hovered');
			}	
		};
		
		for (var i = 0, l = 5; i < l; i++) {
			this.scoreOptions.push({ id: '{0}-{1}'.format(this.name, i + 1), value: i + 1 })	
		}
	},
	template: { element: template }		
});
